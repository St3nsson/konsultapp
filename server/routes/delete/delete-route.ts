import {Request, Response, NextFunction} from "express";
import {data} from "../data";
import * as fs from "fs";
import * as path from "path";
function remove(req: Request, res: Response, next: NextFunction) {
  const id = parseInt(req.params.id, 10);
  if (!id || isNaN(id)) {
    const err = new Error('You need to provide an id');
    return next(err);
  }
  const index = data.findIndex(c => c.id === id);
  let pictureUrl;
  if (index > -1) {
    pictureUrl = data[index].picture;
    data.splice(index, 1);
  }
  if (pictureUrl && pictureUrl !== "") {
    const pathToPic: string = path.join(__dirname, `../../../client/${pictureUrl}`);
    fs.unlink(pathToPic, (err) => {
      if (err) {
        return next(err);
      }
      res.status(200).send();
    });
  } else {
    res.status(200).send();
  }
}

export {remove};
