import {Request, Response, NextFunction} from "express";
import {data} from "../data";

function update(req: Request, res: Response, next: NextFunction) {
  const id = parseInt(req.params.id, 10);
  if (!id || isNaN(id)) {
    const err = new Error('You need to provide an id');
    return next(err);
  }
  const updatedConsultant = req.body;
  updatedConsultant.id = id;
  if(req.file){
    updatedConsultant.picture = './assets/' + req.file.filename;
  }
  updatedConsultant.onAssignment = updatedConsultant.onAssignment === 'true';
  const index = data.findIndex(c => c.id === id);
  if (index > -1) {
    let oldConsultant = data.splice(index, 1, updatedConsultant);
    if(!updatedConsultant.picture){
      updatedConsultant.picture = oldConsultant[0].picture;
    }
    res.send({data: id});
  } else {
    const err = new Error(`no user with the id ${id}`);
    err["status"] = 404;
    next(err);
  }
}

export {update};
