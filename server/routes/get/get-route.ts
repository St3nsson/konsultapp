import {Request, Response, NextFunction} from "express";
import {data} from "../data";

function getAll(req: Request, res: Response) {
  const formattedData = data.map(c => {
    const formattedConsultant = <any>Object.assign({}, c);
    formattedConsultant.age = getAge(formattedConsultant.dateOfBirth);
    return formattedConsultant;
  });
  res.json({data: formattedData});
}

function getById(req: Request, res: Response, next: NextFunction) {
  const id = parseInt(req.params.id, 10);
  if (!id || isNaN(id)) {
    const err = new Error('You need to provide an id');
    err["status"] = 400;
    return next(err);
  }
  const consultant = data.find(c => {
    return c.id === id;
  });
  if (consultant) {
    const cToSend = <any>Object.assign({}, consultant);
    cToSend.age = getAge(cToSend.dateOfBirth);
    res.json({data: cToSend});
  } else {
    const err = new Error(`no user with the id ${id}`);
    err["status"] = 404;
    next(err);
  }
}

function getAge(dob: Date): number {
  if (!(dob instanceof Date)) {
    dob = new Date(dob);
  }
  const today = new Date();
  let age = today.getFullYear() - dob.getFullYear();
  const m = today.getMonth() - dob.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < dob.getDate())) {
    --age;
  }
  return age;
}

export {getAll, getById};
