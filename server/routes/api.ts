import {Router} from "express";
import {getAll, getById} from "./get/get-route";
import {update} from "./update/update-route";
import {add} from "./add/add-route";
import {remove} from "./delete/delete-route";
import * as mime from "mime";
import * as multer from "multer";
import * as path from "path";
const storage = multer.diskStorage({
  destination: (req, file, cb)=>{
    cb(null, path.join(__dirname, "../../client/assets") );
  },
  filename: (req, file, cb)=>{
    console.log(file);
    cb(null, file.fieldname + '-' + Date.now() + '.' + mime.extension(file.mimetype));
  }
});
const upload = multer({storage: storage});

const api: Router = Router();

api.get('/getAll', getAll);
api.get('/getById/:id', getById);
api.post('/update/:id', upload.single('file'), update);
api.post('/add', upload.single('file'), add);
api.delete('/delete/:id', remove);

export {api};
