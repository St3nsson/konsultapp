import {Request, Response} from "express";
import {data} from "../data";

function add(req: Request, res: Response) {
  const newConsultant = req.body;
  if(req.file){
    newConsultant.picture = './assets/' + req.file.filename;
  }

  const highestId = data.reduce((id, c) => c.id > id ? c.id : id, 0);
  newConsultant.id = highestId + 1;
  data.push(newConsultant);

  res.send({data: newConsultant.id});
}

export {add};
