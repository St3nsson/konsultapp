const data = [
  {
    "name": "Gustav Stensson",
    "dateOfBirth": new Date("1990-03-25"),
    "id": 1,
    "email": "st3nsson@gmail.com",
    "address": "Öregrundsgatan 4",
    "skills": [
      "javascript",
      "nodejs"
    ],
    "picture": "./assets/Gustav.jpg",
    "onAssignment": false
  },
  {
    "name": "Donald Trump",
    "dateOfBirth": new Date("1946-06-14"),
    "id": 2,
    "email": "theDonald@gmail.com",
    "address": "1600 Pennsylvania Ave NW, Washington, DC 20500, USA",
    "skills": [
      "Twitter wizard",
      "Wall building",
      "Best at pretty much everything"
    ],
    "picture": "./assets/trump_smile.png",
    "onAssignment": true
  }
];

export {data};
