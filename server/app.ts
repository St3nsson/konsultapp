import * as path from "path";
import * as express from "express";
import {json, urlencoded} from "body-parser";
import {api} from "./routes/api";

const app: express.Application = express();
app.use(json());
app.use(urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, "../client")));

app.use('/api', api);

app.get("*", (req: express.Request, res: express.Response) => {
  const indexPath = path.join(__dirname, '/../client/index.html');
  console.log(indexPath);
  res.sendFile(indexPath);
});


app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
  const err = new Error("Not Found");
  next(err);
});

app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {

  res.status(err.status || 500);
  res.json({
    error: {},
    message: err.message,
  });
});

export {app};
