let config: IEnvSettings;
try {
  const configPath: string = process.env.CONFIG;
  config = require(configPath).envSettings;
} catch (e) {
  config = require('./' + (process.env.NODE_ENV ? process.env.NODE_ENV : 'development')).envSettings;
}

export {config};
