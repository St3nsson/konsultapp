interface IEnvSettings {
  serverPort: number;
  dbPath: string;
}
