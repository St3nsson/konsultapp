import { KonsultappPage } from './app.po';

describe('konsultapp App', () => {
  let page: KonsultappPage;

  beforeEach(() => {
    page = new KonsultappPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
