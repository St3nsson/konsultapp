# Konsultapp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.0.

# Prerequisites

Requires Node 6.9.0 or higher, together with NPM 3 or higher.

# Install

simply run `npm install` to get all dependencies. 

## Development server

Run `npm run serve:dev` for a dev server. Navigate to `http://localhost:4300/`. The app will automatically rebuild if you change any of the source files, and the server will rerun with any server side changes. Then you just have to refresh the browser.
You could run `npm run browser-sync:dev` after `npm run serve:dev`, then you can navigate to `http://localhost:3000/`. This one will refresh automatically on any changes to the angular app.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/client` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
