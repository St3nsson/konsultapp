import {Component, ViewChild, ElementRef} from "@angular/core";
import {Location} from '@angular/common';
import {Consultant} from "../models/Consultant";
import {MdSnackBar, MdSlideToggleChange} from "@angular/material";
import {Router} from "@angular/router";
import {ConsultantService} from "../services/consultant.service";
import {NgForm} from "@angular/forms";
@Component({
  templateUrl: './consultant-form.component.html',
  styleUrls: ['./consultant-form.component.css', '../details/consultant-details.component.css']
})
export class ConsultantFormComponent {
  protected consultant: Consultant;
  protected image;

  @ViewChild('consultantForm')
  form: NgForm;

  @ViewChild('imageEl')
  protected imageEl: ElementRef;

  constructor(protected consultantService: ConsultantService,
              protected router: Router,
              protected location: Location,
              protected mdSnackBar: MdSnackBar) {

  }

  addSkill(skill: string): void {
    if (!this.consultant.skills) {
      this.consultant.skills = [];
    }
    this.consultant.skills = this.consultant.skills.concat([skill]);
    this.form.control.markAsDirty();
  }

  deleteSkill(skill: string): void {
    this.consultant.skills = this.consultant.skills.filter(s => s !== skill);
    this.form.control.markAsDirty();
  }

  imageChanged(): void {
    this.form.control.markAsDirty();
  }

  changeAssignmentStatus(newStatus: MdSlideToggleChange ): void {
    this.form.control.markAsDirty();
    this.consultant.onAssignment = newStatus.checked;
  }

  goBack(): void {
    this.location.back();
  }
}
