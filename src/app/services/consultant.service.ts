import {Consultant} from "../models/Consultant";
import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ConsultantService {
  private cachedConsultants: Consultant[];

  constructor(private http: Http) {
  }

  private clearCache(): void {
    delete this.cachedConsultants;
  }

  private handleError(res: Response) {
    const body = res.json();
    return Observable.throw(body);
  }

  getConsultants(): Observable<Consultant[]> {
    if (this.cachedConsultants) {
      return Observable.of(this.cachedConsultants);
    } else {
      return this.http.get('/api/getall')
        .map(res => {
          const body = res.json();
          this.cachedConsultants = body.data;
          return body.data;
        })
        .catch(this.handleError);
    }
  }

  getConsultant(id: number): Observable<Consultant> {
    if (this.cachedConsultants) {
      const consultant: Consultant = this.cachedConsultants.find(c => c.id === id);
      return Observable.of(consultant);
    } else {
      return this.http
        .get(`/api/getbyid/${id}`)
        .map(res => {
          const body = res.json();
          return body.data;
        })
        .catch(this.handleError);
    }
  }

  editConsultant(consultant: Consultant, files: FileList): Observable<number> {
    let formData: FormData = this.createFormData(consultant, files);

    return this.http
      .post(`/api/update/${consultant.id}`, formData)
      .map(res => {
        this.clearCache();
        const body = res.json();
        return body.data;
      })
      .catch(this.handleError);
  }

  createConsultant(consultant: Consultant, files: FileList): Observable<number> {
    let formData: FormData = this.createFormData(consultant, files);

    return this.http
      .post(`/api/add`, formData)
      .map(res => {
        this.clearCache();
        const body = res.json();
        return <number>body.data;
      })
      .catch(this.handleError);
  }

  deleteConsultant(id: number): Observable<Response> {
    return this.http.delete(`/api/delete/${id}`)
      .map(res => {
        this.clearCache();
        return res;
      });
  }

  private createFormData(consultant: Consultant, files: FileList): FormData {
    let formData: FormData = new FormData();
    if (files) {
      formData.append('file', files.item(0));
    }
    formData.append('name', consultant.name);
    formData.append('dateOfBirth', consultant.dateOfBirth.toString());
    formData.append('address', consultant.address);
    formData.append('email', consultant.email);
    if(consultant.onAssignment){
      formData.append('onAssignment', consultant.onAssignment.toString());
    }
    consultant.skills.forEach(s => formData.append('skills[]', s));

    return formData;
  }

}
