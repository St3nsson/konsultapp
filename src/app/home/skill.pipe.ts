import {PipeTransform, Pipe} from "@angular/core";
import {Consultant} from "../models/Consultant";
@Pipe({name: 'skill'})
export class SkillPipe implements PipeTransform {
  transform(consultants: Consultant[], skills: string[]) {
    if (!skills || skills.length === 0) {
      return consultants;
    }

    return consultants.filter(c => {
      return skills.every(s => {
        return c.skills.map(sk => sk.toLocaleLowerCase()).indexOf(s.toLocaleLowerCase()) > -1;
      });
    });
  }
}
