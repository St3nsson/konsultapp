import {Pipe, PipeTransform} from "@angular/core";
import {Consultant} from "../models/Consultant";

@Pipe({name: 'name'})
export class NamePipe implements PipeTransform {
  transform(consultants: Consultant[], name: string) {
    if (!name || name === "") {
      return consultants;
    }
    return consultants.filter(c => {
      return c.name.toLocaleLowerCase().indexOf(name.toLocaleLowerCase()) > -1;
    });
  }
}
