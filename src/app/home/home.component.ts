import {Component, HostBinding, OnInit} from '@angular/core';
import {ConsultantService} from "../services/consultant.service";
import {Consultant} from "../models/Consultant";
import {flexFadeInOut} from "../animations/flex-fade-in-out";
import {routerTransition} from "../animations/router-transition";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    flexFadeInOut(),
    routerTransition()
  ]
})
export class HomeComponent implements OnInit {
  consultants: Consultant[];
  nameFilter: string;
  skillsFilter: string[];

  constructor(private consultantService: ConsultantService) {
  }

  ngOnInit(): void {
    this.consultantService
      .getConsultants()
      .subscribe(response => {
        this.consultants = response;
      });
  }

  addSkillFilter(skill: string): void {
    if (!this.skillsFilter) {
      this.skillsFilter = [];
    }
    this.skillsFilter = this.skillsFilter.concat([skill]);
  }

  deleteSkillFilter(skill: string): void {
    this.skillsFilter = this.skillsFilter.filter(s => s !== skill);
  }
}
