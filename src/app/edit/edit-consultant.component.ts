import {Component, HostBinding, OnInit} from "@angular/core";
import {Location} from '@angular/common';
import {ConsultantService} from "../services/consultant.service";
import {Router, ActivatedRoute, Params} from "@angular/router";
import {MdSnackBar} from "@angular/material";
import {NgForm} from "@angular/forms";
import {ConsultantFormComponent} from "../form/consultant-form.component";
import {routerTransition} from "app/animations/router-transition";
@Component({
  templateUrl: '../form/consultant-form.component.html',
  styleUrls: ['../form/consultant-form.component.css', '../details/consultant-details.component.css'],
  animations: [
    routerTransition()
  ]
})
export class EditConsultantComponent extends ConsultantFormComponent implements OnInit {
  btnText: string = 'Edit Consultant';
  headerText: string = 'Edit profile info';

  constructor(private activatedRoute: ActivatedRoute,
              consultantService: ConsultantService,
              router: Router,
              location: Location,
              mdSnackBar: MdSnackBar) {
    super(consultantService, router, location, mdSnackBar);
  }

  ngOnInit(): void {
    this.consultant = <any>{};
    this.activatedRoute.params
      .switchMap((params: Params) => this.consultantService.getConsultant(+params['id']))
      .subscribe(consultant => {
        this.consultant = Object.assign({}, consultant);
        this.consultant.skills = consultant.skills.slice(0);
      });
  }

  onSubmit(form: NgForm): void {
    if (!form.form.valid) {
      return;
    }
    const inputEl = this.imageEl.nativeElement;
    const fileCount = inputEl.files.length;
    let files;
    if (fileCount > 0) {
      files = inputEl.files;
    }
    this.consultantService
      .editConsultant(this.consultant, files)
      .subscribe(
        res => {
          this.router.navigate(['/details', res]);
          this.mdSnackBar.open('Edit Sucessfull', '', {duration: 3000, extraClasses: ['success']})
        },
        err => {
          console.log(err);
          this.mdSnackBar.open(err.message, '', {duration: 3000, extraClasses: ['error']});
        }
      );
  }
}
