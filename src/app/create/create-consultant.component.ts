import {Component, HostBinding} from "@angular/core";
import {Location} from '@angular/common';
import {NgForm} from "@angular/forms";
import {ConsultantService} from "../services/consultant.service";
import {Router} from "@angular/router";
import {MdSnackBar} from "@angular/material";
import {ConsultantFormComponent} from "../form/consultant-form.component";
import {routerTransition} from "app/animations/router-transition";
@Component({
  templateUrl: '../form/consultant-form.component.html',
  styleUrls: ['../form/consultant-form.component.css', '../details/consultant-details.component.css'],
  animations: [
    routerTransition()
  ]
})
export class CreateConsultantComponent extends ConsultantFormComponent {
  btnText: string = 'Create Consultant';
  headerText: string = 'Add a new consultant to this great experience';

  constructor(consultantService: ConsultantService,
              router: Router,
              location: Location,
              mdSnackBar: MdSnackBar) {
    super(consultantService, router, location, mdSnackBar);
    this.consultant = <any>{};
  }

  onSubmit(form: NgForm): void {
    if (!form.form.valid) {
      return;
    }
    const inputEl = this.imageEl.nativeElement;
    const fileCount = inputEl.files.length;
    let files;
    if(fileCount > 0){
      files = inputEl.files;
    }
    this.consultantService
      .createConsultant(this.consultant, files)
      .subscribe(
        res => {
          this.router.navigate(['/details', res]);
          this.mdSnackBar.open('Consultant Created', '', {duration: 3000, extraClasses: ['success']});
        },
        err => this.mdSnackBar.open(err.message, '', {duration: 3000, extraClasses: ['error']}));
  }
}
