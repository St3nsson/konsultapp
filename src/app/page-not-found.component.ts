import {Component} from "@angular/core";

@Component({
  template: `
    <div>Ops, seems you've made a wrong turn somewhere</div>`
})
export class PageNotFoundComponent {
}
