import {trigger, state, animate, style, transition} from '@angular/animations';

export function routerTransition() {
  return slideFromRight();
}

function slideFromRight() {
  return trigger('routerTransition', [
    state('*', style({opacity: 1, transform: 'scale(1)'})),
    transition('void => *', [  // enter
      style({opacity: 0, transform: 'scale(0.8)'}),
      animate('0.3s ease-in', style({opacity: 1, transform: 'scale(1)'}))
    ]),
    transition(':leave', [
      style({opacity: 1, transform: 'translateX(0%)'}),
      animate('0.3s linear', style({opacity: 0, transform: 'translateX(100%)'}))
    ])
  ]);
}
