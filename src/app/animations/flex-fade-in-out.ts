import {trigger, state, animate, style, transition} from '@angular/animations';

export function flexFadeInOut() {
  return slideToLeft();
}

function slideToLeft() {
  return trigger('flexFadeInOut', [
    state('*',
      style({opacity: 1, 'flex-wrap': 'wrap', 'flex-grow': 1})),
    transition('void => *', [ // enter
      style({opacity: 0, 'flex-wrap': 'nowrap', 'flex-grow': 0.1}),
      animate('.3s ease-in-out', style({opacity: 1, 'flex-wrap': 'wrap', 'flex-grow': 1}))
    ]),
    transition('* => void', [ // leave
      animate(
        '.3s ease-in-out',
        style({opacity: 0, 'flex-wrap': 'nowrap', 'flex-grow': 0.1}))
    ])
  ])
}
