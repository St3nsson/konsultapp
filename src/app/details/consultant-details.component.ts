import {Component, HostBinding, OnInit} from "@angular/core";
import {Location} from '@angular/common';
import {Consultant} from "../models/Consultant";
import {ConsultantService} from "../services/consultant.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import "rxjs/add/operator/switchMap";
import {routerTransition} from "app/animations/router-transition";
import {MdDialog} from "@angular/material";
@Component({
  templateUrl: './consultant-details.component.html',
  styleUrls: ['./consultant-details.component.css'],
  animations: [
    routerTransition()
  ]
})
export class ConsultantDetailsComponent implements OnInit {
  consultant: Consultant = null;

  constructor(private consultantService: ConsultantService,
              private location: Location,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.consultantService.getConsultant(+params['id']))
      .subscribe(
        consultant => this.consultant = consultant,
        err => this.consultant = undefined
      );
  }

  goBack(): void {
    this.location.back();
  }

  editConsultant(): void {
    this.router.navigate(['/edit', this.consultant.id]);
  }

  deleteConsultant(): void {
    this.consultantService.deleteConsultant(this.consultant.id)
      .subscribe(
        res => this.router.navigate(['/home'])
      )
  }
}
