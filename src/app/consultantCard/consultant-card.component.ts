import {Component, Input} from "@angular/core";
import {Consultant} from "../models/Consultant";
import {Router} from "@angular/router";
@Component({
  selector: 'app-consultant-card',
  templateUrl: './consultant-card.component.html',
  styleUrls: ['./consultant-card.component.css']
})
export class ConsultantCardComponent {
  @Input()
  consultant: Consultant;

  constructor(private router: Router) {
  }

  goToDetails(): void {
    this.router.navigate(['/details', this.consultant.id]);
  }
}
