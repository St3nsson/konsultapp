import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HomeComponent} from "./home/home.component";
import {AppRoutingModule} from "./app-routing.module";
import {PageNotFoundComponent} from "./page-not-found.component";
import {ConsultantCardComponent} from "./consultantCard/consultant-card.component";
import {ConsultantDetailsComponent} from "./details/consultant-details.component";
import {ConsultantService} from "./services/consultant.service";
import {CreateConsultantComponent} from "./create/create-consultant.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {
  MdChipsModule, MdDatepickerModule, MdInputModule, MdNativeDateModule, MdSnackBar,
  MdSnackBarModule, MdTooltipModule, MdSlideToggleModule, MdDialog
} from "@angular/material";
import {FormsModule} from "@angular/forms";
import {NamePipe} from "./home/name.pipe";
import {SkillsInputComponent} from "./skills/skills-input.component";
import {SkillPipe} from "./home/skill.pipe";
import {EditConsultantComponent} from "./edit/edit-consultant.component";
import {ConsultantFormComponent} from "./form/consultant-form.component";
import {HttpModule} from "@angular/http";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
    ConsultantCardComponent,
    ConsultantDetailsComponent,
    ConsultantFormComponent,
    EditConsultantComponent,
    CreateConsultantComponent,
    SkillsInputComponent,
    SkillPipe,
    NamePipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    MdInputModule,
    MdNativeDateModule,
    MdDatepickerModule,
    MdChipsModule,
    MdSnackBarModule,
    MdTooltipModule,
    MdSlideToggleModule,
    AppRoutingModule
  ],
  providers: [ConsultantService, MdSnackBar],
  bootstrap: [AppComponent]
})
export class AppModule { }
