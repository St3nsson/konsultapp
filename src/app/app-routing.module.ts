import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {PageNotFoundComponent} from "./page-not-found.component";
import {ConsultantDetailsComponent} from "./details/consultant-details.component";
import {CreateConsultantComponent} from "./create/create-consultant.component";
import {EditConsultantComponent} from "./edit/edit-consultant.component";

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'create', component: CreateConsultantComponent},
  {path: 'edit/:id', component: EditConsultantComponent},
  {path: 'details/:id', component: ConsultantDetailsComponent},
  {path: '**', component: PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
