export class Consultant {
  id: number;
  name: string;
  age: number;
  dateOfBirth: Date;
  email: string;
  address: string;
  skills: string[];
  picture: string;
  onAssignment: boolean;
}
