import {Component, Input, Output, EventEmitter, OnChanges, SimpleChange} from "@angular/core";
@Component({
  selector: 'app-skills-input',
  templateUrl: './skills-input.component.html',
  styleUrls: ['./skills-input.component.css'],
})
export class SkillsInputComponent implements OnChanges {
  @Input()
  skills: string[];
  @Input()
  placeholder: string;

  currentSkill: string;
  @Output() onAdd = new EventEmitter<string>();
  @Output() onDelete = new EventEmitter<string>();

  ngOnChanges(changes: {[propKey: string]: SimpleChange}){
    if(changes.skills && !changes.skills.isFirstChange()){
      this.skills = changes.skills.currentValue;
    }
  }

  addSkill($event ?: KeyboardEvent): void{
    if ($event) {
      if ($event.which === 13) {// enter
        $event.stopImmediatePropagation();
        $event.preventDefault();
      } else {
        return;
      }
    }
    if (!this.currentSkill || this.currentSkill === "") {
      return;
    }

    if (this.skills && this.skills.indexOf(this.currentSkill) > -1) {
      return;
    }

    this.onAdd.emit(this.currentSkill);
    this.currentSkill = "";
  }

  deleteSkill(skill: string):void {
    this.onDelete.emit(skill);
  }
}
